package com.example.rosa;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.rosa.constans.Constans;
import com.firebase.ui.firestore.FirestoreRecyclerAdapter;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;

public class RosaAdapter extends FirestoreRecyclerAdapter<ROSAModel, RosaAdapter.RosaHolder> {

    public RosaAdapter(@NonNull FirestoreRecyclerOptions<ROSAModel> options) {
        super(options);
    }

    @Override
    protected void onBindViewHolder(@NonNull final RosaHolder holder, int position, @NonNull final ROSAModel model) {
        holder.textViewTitle.setText(model.getRosaName());
        holder.textViewNumberOfUsers.setText(model.getNumberOfUsers());
        holder.textViewAmount.setText(model.getAmount());
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Constans.rosaModel = model;
                Intent intent = new Intent(holder.itemView.getContext(), RosaDetails.class);
                holder.itemView.getContext().startActivity(intent);

            }
        });
    }

    @NonNull
    @Override
    public RosaHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.layout_listitem, viewGroup, false);
        return new RosaHolder(v);
    }

    class RosaHolder extends RecyclerView.ViewHolder {

        TextView textViewTitle;
        TextView textViewNumberOfUsers;
        TextView textViewAmount;

        public RosaHolder(@NonNull final View itemView) {
            super(itemView);
            textViewTitle = itemView.findViewById(R.id.textViewTitle);
            textViewNumberOfUsers = itemView.findViewById(R.id.textViewNumberOfUsers);
            textViewAmount = itemView.findViewById(R.id.textViewAmount);

        }
    }
}
