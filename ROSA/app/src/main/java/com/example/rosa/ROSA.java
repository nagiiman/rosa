package com.example.rosa;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.rosa.constans.Constans;
import com.example.rosa.model.ChatModel;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import co.intentservice.chatui.models.ChatMessage;

public class ROSA extends AppCompatActivity implements View.OnClickListener {

    private ImageView imageViewBack;
    private Button buttonCreateROSA;
    private EditText editTextROSAName, sdate, edate;
    private SeekBar seekBarUsers;
    private SeekBar seekBarAmount;
    private SeekBar seekBarPosition;
    private RadioButton radioButtonCard;
    private RadioButton radioButtonCash;
    private TextView textViewNameROSA;
    private TextView textViewUsers;
    private TextView textViewUserAmount;
    private TextView textViewPosition;
    private String numberOfUsers;
    private int max;
    private FirebaseFirestore firebaseFirestore;
    private FirebaseAuth firebaseAuth;
    public String amount;
    private static final String TAG = "ROSA";

    private User adminUser;
    private List<User> users;
    private String test;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rosa);
        users = new ArrayList<>();
        users.add(null);
        users.add(null);
        users.add(null);
        users.add(null);
        users.add(null);
        imageViewBack = (ImageView) findViewById(R.id.imageViewBack);
        buttonCreateROSA = (Button) findViewById(R.id.buttonCreateROSA);
        editTextROSAName = (EditText) findViewById(R.id.editTextROSAName);
        seekBarUsers = (SeekBar) findViewById(R.id.seekBarUsers);
        seekBarUsers.setMax(3);
        seekBarAmount = (SeekBar) findViewById(R.id.seekBarAmount1);
        seekBarAmount.setMax(20);
        seekBarPosition = (SeekBar) findViewById(R.id.seekBarPosition);
        //seekBarAmount.setProgress(20);
        //seekBarAmount.setProgress(5);

        edate = (EditText) findViewById(R.id.eedate);
        sdate = (EditText) findViewById(R.id.sdate);
        textViewUsers = (TextView) findViewById(R.id.textViewUser1);
        textViewUserAmount = (TextView) findViewById(R.id.textViewUserAmount1);
        textViewPosition = (TextView) findViewById(R.id.textViewPosition1);
        firebaseFirestore = firebaseFirestore.getInstance();
        firebaseAuth = FirebaseAuth.getInstance();

        seekBarUsers.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                //int val = (progress * (seekBar.getWidth() - 2 * seekBarUsers.getThumbOffset()) + 2)  / seekBarUsers.getMax() ;
                textViewUsers.setText("" + (progress + 3));
                numberOfUsers = textViewUsers.getText().toString();
                max = Integer.parseInt(numberOfUsers);
                seekBarPosition.setMax(max);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
        //
        seekBarAmount.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                textViewUserAmount.setText("" + "£" + ((progress * 5)));
                amount = "" + progress * 5;
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });


        seekBarPosition.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                int min = 1;
                if (progress < min) {
                    textViewPosition.setText("" + min);
                } else {

                    textViewPosition.setText("" + progress);

                }

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        radioButtonCard = (RadioButton) findViewById(R.id.radioButtonCard);
        radioButtonCash = (RadioButton) findViewById(R.id.radioButtonCash);

        imageViewBack.setOnClickListener(this);
        buttonCreateROSA.setOnClickListener(this);


    }

    private void saveUserInformation() {
        //Map<User, Integer> userPositions = new HashMap<>();
        gettingData();
        //gettingCurrentUser();
        //users.add(adminUser);
    }

    private void creatingROSA(String rosaName, String numberOfUsers, String amount, Boolean cash, String startdate, String enddate, String position) {

        // Log.d(TAG, "Document to user rosa " + users.get(0).FirstName1);
        List<ChatModel> chat = new ArrayList<>();
        Timestamp timestamp = new Timestamp(System.currentTimeMillis());
        chat.add(new ChatModel("Wellcome to ROSA chat", ChatMessage.Type.RECEIVED, timestamp.getTime(), "ROSA"));
        ROSAModel rosaModel = new ROSAModel(rosaName, numberOfUsers, amount, cash, users, startdate, enddate, position, Constans.user, chat, false, false, 1);
        Constans.user.createdRosa = rosaName;
        firebaseFirestore.collection("rosa").document(rosaName).set(rosaModel).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                Log.d(TAG, "DocumentSnapshot successfully written!");
                Toast.makeText(ROSA.this, "Registered Successfully", Toast.LENGTH_SHORT).show();
                nextActivity();
            }
        })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.w(TAG, "Error writing document", e);
                        Toast.makeText(ROSA.this, "Failed Registration: " + e.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });
    }


    private void gettingData() {
        FirebaseUser user = firebaseAuth.getCurrentUser();
        String userEmail = user.getEmail();
        final DocumentReference docRef = firebaseFirestore.collection("users").document(userEmail);
        final String docPath = docRef.getPath();
        docRef.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                if (task.isSuccessful()) {
                    DocumentSnapshot document = task.getResult();
                    if (document.exists()) {
                        Log.d(TAG, "Document Snapshot data: " + document.getData());

                        String firstName = document.getData().get("FirstName1").toString();
                        String lastName = document.getData().get("LastName1").toString();
                        String emaail = document.getData().get("EmailAddress1").toString();
                        String phone = document.getData().get("Phonenumber1").toString();
                        String dob = document.getData().get("DOB1").toString();
                        String add = document.getData().get("Address11").toString();
                        String add2 = document.getData().get("Address21").toString();
                        String city = document.getData().get("City1").toString();
                        String country = document.getData().get("Country1").toString();

                        List<String> joindRosa = (List<String>) document.getData().get("joinedRosa");
                        Log.d(TAG, "Document to user " + firstName + " " + lastName + " " + emaail + " " + phone + " " + dob + " " + add + " " + add2 + " " + city + " " + country);
                        String rating = document.getData().get("rating").toString();
                        String rosaName = editTextROSAName.getText().toString();
                        String numberOfUsers = textViewUsers.getText().toString();
                        String amount = ROSA.this.amount;
                        Boolean cash = radioButtonCash.isChecked();
                        String position = textViewPosition.getText().toString();
                        String startdate = sdate.getText().toString();
                        String enddate = edate.getText().toString();
                        String createdRosa = rosaName;
                        joindRosa.add(rosaName);

                        User user = new User(firstName, emaail, lastName, phone, add, add2, city, country, dob, position, joindRosa, createdRosa, rating);
                        firebaseFirestore.collection("users").document(emaail).set(user).addOnSuccessListener(new OnSuccessListener<Void>() {
                            @Override
                            public void onSuccess(Void aVoid) {
                                Log.d(TAG, "DocumentSnapshot successfully written!");
                                Toast.makeText(ROSA.this, "Registered Successfully", Toast.LENGTH_SHORT).show();
                                nextActivity();
                            }
                        })
                                .addOnFailureListener(new OnFailureListener() {
                                    @Override
                                    public void onFailure(@NonNull Exception e) {
                                        Log.w(TAG, "Error writing document", e);
                                        Toast.makeText(ROSA.this, "Failed Registration: " + e.getMessage(), Toast.LENGTH_SHORT).show();
                                    }
                                });

                        Constans.user = user;

                        users.add(Integer.parseInt(position) - 1, user);

                        // Log.d(TAG, "Users" + users.get(0).FirstName1);

                        creatingROSA(rosaName, numberOfUsers, amount, cash, startdate, enddate, position);

                        Log.d(TAG, "User" + user.FirstName1);
                        //textViewFirstName.setText("Welcome " + firstName);
                    } else {
                        Log.d(TAG, "No such document");
                        //    Toast.makeText(Home.this, "Failed Registration:2 "+task.getException(), Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Log.d(TAG, "get failed with ", task.getException());
                    //Toast.makeText(Home.this, "Failed Registration:3 "+task.getException(), Toast.LENGTH_SHORT).show();
                }
            }
        });
    }


    /*
       private void gettingCurrentUser(){
           String currentUserEmail = firebaseAuth.getCurrentUser().getEmail();
           DocumentReference docRef = firebaseFirestore.collection("users").document(currentUserEmail);
           docRef.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
               @Override
               public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                   if(task.isSuccessful()) {
                       DocumentSnapshot document = task.getResult();
                       if(document.exists()) {

                           test = document.getString("FirstName");

                       }
                       else {
                          Log.e(TAG, "Error message", task.getException());
                       }
                   }
               }
           });
       }
   */
    @Override
    public void onClick(View v) {
        if (v == imageViewBack) {
            startActivity(new Intent(this, Home.class));
        }
        if (v == buttonCreateROSA) {
            saveUserInformation();
            startActivity(new Intent(this, Home.class));
        }

    }

    private void nextActivity() {
        finish();
        startActivity(new Intent(this, Home.class));
    }
}
























/*
 private void gettingCurrentUser(){
        String currentUserEmail = firebaseAuth.getCurrentUser().getEmail();
        DocumentReference docRef = firebaseFirestore.collection("users").document(currentUserEmail);
        docRef.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                if(task.isSuccessful()) {
                    DocumentSnapshot document = task.getResult();
                    if(document.exists()) {

                        test = document.getString("FirstName");

                    }
                }
            }
        });
    }
 */








  /*            String firstName = documentSnapshot.getData().get("FirstName").toString();
                String lastName = documentSnapshot.getData().get("LastName").toString();
                String emailAddress = documentSnapshot.getData().get("EmailAddress").toString();
                String dob = documentSnapshot.getData().get("DOB").toString();
                String address1 = documentSnapshot.getData().get("Address1").toString();
                String address2 = documentSnapshot.getData().get("Address2").toString();
                String city = documentSnapshot.getData().get("City").toString();
                String country = documentSnapshot.getData().get("Country").toString();
                String phonenumber = documentSnapshot.getData().get("Phonenumber").toString();

                User user = new User(firstName, emailAddress, lastName, phonenumber, address1, address2, city, country,dob);

                adminUser = user;
*/







           /* @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                if (task.isSuccessful()) {
                    DocumentSnapshot document = task.getResult();
                    if(document.exists()) {
                        String firstName = document.getData().get("FirstName").toString();
                        String lastName = document.getData().get("LastName").toString();
                        String emailAddress = document.getData().get("EmailAddress").toString();
                        String dob = document.getData().get("DOB").toString();
                        String address1 = document.getData().get("Address1").toString();
                        String address2 = document.getData().get("Address2").toString();
                        String city = document.getData().get("City").toString();
                        String country = document.getData().get("Country").toString();
                        String phonenumber = document.getData().get("Phonenumber").toString();

                        User temp = new User(firstName, emailAddress, lastName, phonenumber, address1, address2, city, country,dob);

                        adminUser = temp;
                    } else {
                        Log.d(TAG, "No such document");
                        //    Toast.makeText(Home.this, "Failed Registration:2 "+task.getException(), Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Log.d(TAG, "get failed with ", task.getException());
                    //Toast.makeText(Home.this, "Failed Registration:3 "+task.getException(), Toast.LENGTH_SHORT).show();
                }
            }
        });
       /* firebaseFirestore.collection("users").document(currentUserEmail).get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                if (task.isSuccessful()) {
                    DocumentSnapshot document = task.getResult();
                    if (document.exists()) {
                        Log.d(TAG, "DocumentSnapshot data: " + document.getData());
                       //String uid = document.getData().get("uid").toString();
                       //test = document.getData().get("FirstName").toString();

                       //String lastName = document.getData().get("LastName").toString();
                       //String emailAddress = document.getData().get("EmailAddress").toString();
                       //String dob = document.getData().get("DOB").toString();
                       //String address1 = document.getData().get("Address1").toString();
                       //String address2 = document.getData().get("Address2").toString();
                       //String city = document.getData().get("City").toString();
                       //String country = document.getData().get("Country").toString();
                       //String phonenumber = document.getData().get("Phonenumber").toString();

                       //User currentUser = new User(firstName, emailAddress, lastName, phonenumber, address1, address2, city, country,dob);
                      // users.add(currentUser);
                        //adminUser = currentUser;

                        adminUser = document.toObject(User.class);

                    } else {
                        Log.d(TAG, "No such document");
                        FirebaseFirestoreException e = (FirebaseFirestoreException) task.getException();
                        Toast.makeText(ROSA.this, "Failed Registration: "+e.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Log.d(TAG, "get failed with ", task.getException());
                }
            }
        });
    */
    