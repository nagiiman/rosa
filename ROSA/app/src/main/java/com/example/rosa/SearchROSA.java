package com.example.rosa;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.firebase.ui.firestore.paging.FirestoreDataSource;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;

public class SearchROSA extends AppCompatActivity implements View.OnClickListener {

    private EditText monthlyAmount;
    private EditText userNumbers;
    private EditText preferredPosition;
    private Button buttonSearch;
    private FirebaseFirestore firebaseFirestore =  FirebaseFirestore.getInstance();
    private CollectionReference rosaRef = firebaseFirestore.collection("rosa");
    private ROSASearchAdapter adapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_ros);

        monthlyAmount = (EditText) findViewById(R.id.editTextAmount);
        userNumbers = (EditText) findViewById(R.id.editTextUserNumbers);
        preferredPosition = (EditText) findViewById(R.id.editTextPosition);
        buttonSearch = (Button) findViewById(R.id.buttonSearch);
        buttonSearch.setOnClickListener(this);

       setUpRecyclerView();
    }

    private void setUpRecyclerView() {
        String searchAmount = monthlyAmount.getText().toString();
        Query searchQuery = rosaRef.whereEqualTo("amount",searchAmount);
        Query memeberQuery= rosaRef.whereEqualTo("numberOfUsers",userNumbers.getText().toString());
        Query preferdpostion= rosaRef.whereEqualTo("numberOfUsers",userNumbers.getText().toString());

        FirestoreRecyclerOptions<ROSAModel> options = new FirestoreRecyclerOptions.Builder<ROSAModel>()
                .setQuery(searchQuery, ROSAModel.class).build();



        adapter = new ROSASearchAdapter(options);
        RecyclerView recyclerView = findViewById(R.id.recycleViewSearchROSA);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        recyclerView.setAdapter(adapter);
    }

    @Override
    public void onClick(View v) {
        if (v == buttonSearch) {
            setUpRecyclerView();
            adapter.startListening();
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        adapter.startListening();
    }

    @Override
    protected void onStop() {
        super.onStop();
        adapter.stopListening();
    }

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }

    public void gopreferedperson(View view){

        String searchAmount = monthlyAmount.getText().toString();
        Query searchQuery = rosaRef.whereEqualTo("amount",searchAmount);

        FirestoreRecyclerOptions<ROSAModel> options = new FirestoreRecyclerOptions.Builder<ROSAModel>()
                .setQuery(searchQuery, ROSAModel.class)
                .build();



        adapter = new ROSASearchAdapter(options);
        RecyclerView recyclerView = findViewById(R.id.recycleViewSearchROSA);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        recyclerView.setAdapter(adapter);
        adapter.startListening();

       adapter.notifyDataSetChanged();

    }

    public void goamount(View view){

        String searchAmount = monthlyAmount.getText().toString();
        Query searchQuery = rosaRef.whereEqualTo("amount",searchAmount);

        FirestoreRecyclerOptions<ROSAModel> options = new FirestoreRecyclerOptions.Builder<ROSAModel>()
                .setQuery(searchQuery, ROSAModel.class)
                .build();



        adapter = new ROSASearchAdapter(options);
        RecyclerView recyclerView = findViewById(R.id.recycleViewSearchROSA);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        recyclerView.setAdapter(adapter);
        adapter.startListening();

        adapter.notifyDataSetChanged();
    }

    public void gonuser(View view){
        String searchAmount = monthlyAmount.getText().toString();
        Query searchQuery = rosaRef.whereEqualTo("amount",searchAmount);
        Query memeberQuery= rosaRef.whereEqualTo("numberOfUsers",userNumbers.getText().toString());
        Query preferdpostion= rosaRef.whereEqualTo("numberOfUsers",userNumbers.getText().toString());

        FirestoreRecyclerOptions<ROSAModel> options = new FirestoreRecyclerOptions.Builder<ROSAModel>()
                .setQuery(memeberQuery, ROSAModel.class)
                .build();


        adapter = new ROSASearchAdapter(options);
        RecyclerView recyclerView = findViewById(R.id.recycleViewSearchROSA);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        recyclerView.setAdapter(adapter);
        adapter.startListening();
         adapter.notifyDataSetChanged();

    }
}