package com.example.rosa;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import com.example.rosa.constans.Constans;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.FirebaseFirestore;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;


public class Settings extends Fragment {
    ProgressDialog progressDialog;
    private FirebaseFirestore firebaseFirestore;
    final Calendar myCalendar = Calendar.getInstance();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        progressDialog = new ProgressDialog(getContext());
        progressDialog.setMessage("Updating info. Please wait..");
        progressDialog.setCanceledOnTouchOutside(false);
        firebaseFirestore = firebaseFirestore.getInstance();

        View view = inflater.inflate(R.layout.fragment_settings, container, false);
        ImageButton csd = view.findViewById(R.id.sd);
        ImageButton cp = view.findViewById(R.id.cp);
        ImageButton lr = view.findViewById(R.id.lr);
        ImageButton rate = view.findViewById(R.id.rate);
        final ImageButton ready = view.findViewById(R.id.ready);

        if (Constans.rosaModel.joineduser == 6 && !Constans.rosaModel.started) {
            ready.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    readyToStart(v);
                }
            });


        } else {
            ready.setEnabled(false);


        }


        if (Constans.rosaModel.complete) {

            rate.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    rate(v);
                }
            });

        } else {

            rate.setEnabled(false);
        }
        csd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                changeStartDate(v);
            }
        });
        cp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                changePosition(v);
            }
        });
        lr.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                leaveRosa(v);
            }
        });
        return view;

    }

    public void changeStartDate(View view) {
        startActivity(new Intent(getContext(), EditStartDate.class));

    }

    public void changePosition(View view) {
        startActivity(new Intent(getContext(), EditPosition.class));


    }


    public void leaveRosa(View view) {
        progressDialog.show();
        for (int i = 0; i < Constans.rosaModel.UserPositions.size(); i++) {

            User user = Constans.rosaModel.UserPositions.get(i);
            if (user != null) {
                if (user.FirstName1.equalsIgnoreCase(Constans.user.FirstName1) && user.LastName1.equalsIgnoreCase(Constans.user.LastName1)) {
                    Constans.rosaModel.UserPositions.remove(i);
                    Constans.rosaModel.UserPositions.add(i, null);
                    Constans.rosaModel.joineduser -= 1;

                }
            }

        }

        for (int i = 0; i < Constans.user.joinedRosa.size(); i++) {
            if (Constans.user.joinedRosa.get(i).equalsIgnoreCase(Constans.rosaModel.rosaName)) {

                Constans.user.joinedRosa.remove(i);

            }


        }

        firebaseFirestore.collection("rosa").document(Constans.rosaModel.rosaName).set(Constans.rosaModel).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                //Log.d(TAG, "DocumentSnapshot successfully written!");
                Toast.makeText(Settings.this.getContext(), "You have Successfully leave ROSA " + Constans.rosaModel.rosaName, Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();

            }
        })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        //      Log.w(TAG, "Error writing document", e);
                        Toast.makeText(Settings.this.getContext(), "Failed: " + e.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });

        firebaseFirestore.collection("users").document(Constans.user.EmailAddress1).set(Constans.user).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                //Toast.makeText(Settings.this.getContext(), " Successfully", Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();
                getActivity().onBackPressed();

            }
        })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Toast.makeText(Settings.this.getContext(), "Failed Registration: " + e.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });


    }

    public void rate(View view) {
        startActivity(new Intent(getContext(), RateUser.class));

    }

    public void readyToStart(View v) {

        Dialog dialog = new Dialog(Settings.this.getContext());
        dialog.setContentView(R.layout.start_date_agreement);
        final EditText edate = dialog.findViewById(R.id.date);
        Button start = dialog.findViewById(R.id.start);

        start.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                progressDialog = new ProgressDialog(getContext());
                progressDialog.setMessage("Updating info. Please wait..");
                progressDialog.setCanceledOnTouchOutside(false);
                progressDialog.show();
                firebaseFirestore = firebaseFirestore.getInstance();
                if (!edate.getText().toString().isEmpty()) {
                    firebaseFirestore.collection("rosa").document(Constans.rosaModel.rosaName).set(Constans.rosaModel).addOnSuccessListener(new OnSuccessListener<Void>() {
                        @Override
                        public void onSuccess(Void aVoid) {
                            //Log.d(TAG, "DocumentSnapshot successfully written!");
                            Toast.makeText(Settings.this.getContext(), "You have Successfully leave ROSA " + Constans.rosaModel.rosaName, Toast.LENGTH_SHORT).show();
                            progressDialog.dismiss();

                        }
                    })
                            .addOnFailureListener(new OnFailureListener() {
                                @Override
                                public void onFailure(@NonNull Exception e) {
                                    //      Log.w(TAG, "Error writing document", e);
                                    Toast.makeText(Settings.this.getContext(), "Failed: " + e.getMessage(), Toast.LENGTH_SHORT).show();
                                }
                            });

                } else {

                    Toast.makeText(Settings.this.getContext(), "Insert date ", Toast.LENGTH_SHORT).show();

                }


            }
        });
        final DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);

                String myFormat = "MM/dd/yy"; //In which you need put here
                SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);

                edate.setText(sdf.format(myCalendar.getTime()));

            }

        };


        edate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                new DatePickerDialog(Settings.this.getContext(), date, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();

            }
        });


    }


}
