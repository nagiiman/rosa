package com.example.rosa.model;

import com.example.rosa.Chat;

import co.intentservice.chatui.models.ChatMessage;

public class ChatModel {
    public String message;
    public ChatMessage.Type type;
    public long time;
    public String sender;
  public ChatModel(){}

    public ChatModel(String message, ChatMessage.Type type, long time,String sender) {
        this.message = message;
        this.type = type;
        this.time = time;
        this.sender=sender;
    }
}
