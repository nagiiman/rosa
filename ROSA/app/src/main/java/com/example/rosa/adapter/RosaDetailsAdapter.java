package com.example.rosa.adapter;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.example.rosa.Chat;
import com.example.rosa.Settings;
import com.example.rosa.Summery;

public class RosaDetailsAdapter extends FragmentPagerAdapter {
    private Context myContext;
    int totalTabs;

    public RosaDetailsAdapter(Context context, FragmentManager fm, int totalTabs) {
        super(fm);
        myContext = context;
        this.totalTabs = totalTabs;
    }

    // this is for fragment tabs
    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                Summery summeryFragment = new Summery();
                return summeryFragment;
            case 1:
                Chat chatFragment = new Chat();
                return chatFragment;
            case 2:
                Settings settingsFragment = new Settings();
                return settingsFragment;
            default:
                return null;
        }
    }
    // this counts total number of tabs
    @Override
    public int getCount() {
        return totalTabs;
    }



}
