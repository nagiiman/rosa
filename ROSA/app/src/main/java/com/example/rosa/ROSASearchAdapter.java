package com.example.rosa;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.rosa.constans.Constans;
import com.firebase.ui.firestore.FirestoreRecyclerAdapter;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;

public class ROSASearchAdapter extends FirestoreRecyclerAdapter<ROSAModel, ROSASearchAdapter.RosaSearchHolder> {

    public ROSASearchAdapter(@NonNull FirestoreRecyclerOptions<ROSAModel> options) {
        super(options);
    }

    @Override
    protected void onBindViewHolder(@NonNull final RosaSearchHolder holder, int position, @NonNull final ROSAModel model) {
        holder.textViewTitle.setText(model.getRosaName());
        holder.textViewNumberOfUsers.setText(model.getNumberOfUsers());
        holder.textViewAmount.setText(model.getAmount());
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (v == holder.itemView) {

                    Constans.searchRosaModel=model;
                    Intent intent = new Intent(holder.itemView.getContext(), ROSAGroup.class);
                    intent.putExtra("title", holder.textViewTitle.getText());
                    holder.itemView.getContext().startActivity(intent);



                }
            }
        });


    }

    @NonNull
    @Override
    public RosaSearchHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.layout_search_rosa, viewGroup, false);

        return new RosaSearchHolder(v);
    }

    class RosaSearchHolder extends RecyclerView.ViewHolder {

        TextView textViewTitle;
        TextView textViewNumberOfUsers;
        TextView textViewAmount;

        public RosaSearchHolder(@NonNull final View itemView) {
            super(itemView);
            textViewTitle = itemView.findViewById(R.id.textViewTitleSearch);
            textViewNumberOfUsers = itemView.findViewById(R.id.textViewNumberOfUsersSearch);
            textViewAmount = itemView.findViewById(R.id.textViewAmountSearch);

        }
    }
}
