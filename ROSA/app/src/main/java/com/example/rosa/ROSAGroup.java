package com.example.rosa;

import android.app.ProgressDialog;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.rosa.constans.Constans;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.FirebaseFirestore;

import java.io.InterruptedIOException;
import java.util.ArrayList;
import java.util.List;

public class ROSAGroup extends AppCompatActivity implements View.OnClickListener{

    private TextView textViewROSAName;
    private TextView textViewCreatedBy;
    private TextView textViewNumberOfUsers;
    private TextView textViewAvailableSpaces;
    private TextView textViewExpectedStartDate;
    private TextView textViewExpectedEndDate;
    private TextView textViewMonthlyAmount;
    private TextView textViewPooledAmount;
    private Button buttonJoinROSA;
    private FirebaseFirestore firebaseFirestore;
     private List<Integer> availableSpaces=new ArrayList<>();
    private TextView textView;
     ProgressDialog progressDialog;
    private String TAG="Firebase";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rosagroup);

        firebaseFirestore = firebaseFirestore.getInstance();

        progressDialog=new ProgressDialog(this);
        progressDialog.setMessage("Getting informaiton Please Wait..");
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();
        textViewROSAName = (TextView) findViewById(R.id.textViewROSAName);
        textViewCreatedBy = (TextView) findViewById(R.id.textViewCreatorName);
        textViewNumberOfUsers = (TextView) findViewById(R.id.textViewUserNumber);
        textViewAvailableSpaces = (TextView) findViewById(R.id.textViewAvailableSpaces);
        textViewExpectedStartDate = (TextView) findViewById(R.id.textViewStartDate);
        textViewExpectedEndDate = (TextView) findViewById(R.id.textViewEndDate);
        textViewMonthlyAmount = (TextView) findViewById(R.id.textViewMonthlyAmount);
        textViewPooledAmount = (TextView) findViewById(R.id.textViewPooledAmount);
        buttonJoinROSA = (Button) findViewById(R.id.buttonJoinROSA);

        buttonJoinROSA.setOnClickListener(this);

        textView = (TextView) findViewById(R.id.textViewTitle);

        textViewROSAName.setText(Constans.searchRosaModel.rosaName);
        System.out.println("User test "+Constans.searchRosaModel.user);
        textViewCreatedBy.setText(Constans.searchRosaModel.user.FirstName1+" "+Constans.searchRosaModel.user.LastName1);
        textViewNumberOfUsers.setText(Constans.searchRosaModel.numberOfUsers);
        for(int i=1;i<=Constans.searchRosaModel.UserPositions.size();i++){
            if(Constans.searchRosaModel.UserPositions.get(i-1)==null){

                availableSpaces.add(i-1);
                textViewAvailableSpaces.append(i+", ");


            }


        }
textViewExpectedStartDate.setText(Constans.searchRosaModel.startdate);
     textViewExpectedEndDate.setText(Constans.searchRosaModel.enddate);
     textViewMonthlyAmount.setText("£"+Constans.searchRosaModel.amount);
     textViewPooledAmount.setText("£"+Integer.parseInt(Constans.searchRosaModel.amount)*Integer.parseInt(Constans.searchRosaModel.numberOfUsers));
     progressDialog.dismiss();
        //   getIncomingIntents();
    }

    private void getIncomingIntents() {
        if(getIntent().hasExtra("title")) {

            String rosaTitle = getIntent().getStringExtra("title");

            textViewROSAName.setText(rosaTitle);
        }
    }

    @Override
    public void onClick(View v) {

        progressDialog.setMessage("Joining "+Constans.searchRosaModel.rosaName+" Please Wait..");
       progressDialog.show();
boolean nextFlag=true;
if(Constans.searchRosaModel.joineduser<6) {
    for (User user : Constans.searchRosaModel.UserPositions) {


        if (user != null && user.FirstName1.equalsIgnoreCase(Constans.user.FirstName1) && user.LastName1.equalsIgnoreCase(Constans.user.LastName1)) {
            nextFlag = false;
            Toast.makeText(this, "You have already joined ROSA", Toast.LENGTH_SHORT).show();
            progressDialog.dismiss();

        }
    }
    if (nextFlag) {
        if (Constans.user.joinedRosa != null) {
            Constans.user.joinedRosa.add(Constans.searchRosaModel.rosaName);

        } else {
            Constans.user.joinedRosa = new ArrayList<>();
            Constans.user.joinedRosa.add(Constans.searchRosaModel.rosaName);

        }


        List<User> users = new ArrayList<>();
        int i = 0;
        for (User user : Constans.searchRosaModel.UserPositions) {


            if (i == availableSpaces.get(0)) {
                users.add(Constans.user);

            } else if (user == null) {

                users.add(null);
            } else {
                users.add(user);

            }

            i++;
        }
        Constans.searchRosaModel.joineduser+=1;
        Constans.searchRosaModel.UserPositions = users;


        firebaseFirestore.collection("rosa").document(Constans.searchRosaModel.rosaName).set(Constans.searchRosaModel).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                Log.d(TAG, "DocumentSnapshot successfully written!");
                Toast.makeText(ROSAGroup.this, "Successfully Joined " + Constans.searchRosaModel.rosaName, Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();
            }
        })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.w(TAG, "Error writing document", e);
                        Toast.makeText(ROSAGroup.this, "Failed Registration: " + e.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });
        firebaseFirestore.collection("users").document(Constans.user.EmailAddress1).set(Constans.user).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                Log.d(TAG, "DocumentSnapshot successfully written!");

                Toast.makeText(ROSAGroup.this, "Registered Successfully", Toast.LENGTH_SHORT).show();
                onBackPressed();
            }
        })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.w(TAG, "Error writing document", e);
                        Toast.makeText(ROSAGroup.this, "Failed Registration: " + e.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });


    }
}
    }
}
