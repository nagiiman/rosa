package com.example.rosa;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

public class FirstRegistration extends AppCompatActivity  implements  View.OnClickListener {

    private EditText editTextEmailReg;
    private EditText editTextPasswordReg;
    private EditText editTextConfirmPassReg;

    private Button buttonRegister;

    private ProgressDialog progressDialog;
    private FirebaseAuth firebaseAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_first_registration);

        firebaseAuth = FirebaseAuth.getInstance();
        progressDialog = new ProgressDialog(this);

        editTextEmailReg = (EditText) findViewById(R.id.editTextEmailReg);
        editTextPasswordReg = (EditText) findViewById(R.id.editTextPasswordReg);
        editTextConfirmPassReg = (EditText) findViewById(R.id.editTextConfirmPassReg);

        buttonRegister = (Button) findViewById(R.id.buttonRegister);
        buttonRegister.setOnClickListener(this);
    }

    private void registerUser() {
        String email = editTextEmailReg.getText().toString().trim();
        String password = editTextPasswordReg.getText().toString().trim();
        String confirmPass = editTextConfirmPassReg.getText().toString().trim();

        if (TextUtils.isEmpty(email)) {
            Toast.makeText(this, "Please enter your email address.", Toast.LENGTH_SHORT).show();
            return;
        }
        if (TextUtils.isEmpty(password) && TextUtils.isEmpty(confirmPass)) {
            Toast.makeText(this, "Please enter your password.", Toast.LENGTH_SHORT).show();
            return;
        }

        if (!checkPassword(password, confirmPass)) {
            Toast.makeText(this, "Your Passwords do not match. Please enter your passwords again.", Toast.LENGTH_SHORT).show();
            return;
        }

        progressDialog.setMessage("Registering User...");
        progressDialog.show();

        firebaseAuth.createUserWithEmailAndPassword(email, password).addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
         @Override
        public void onComplete(@NonNull Task<AuthResult> task) {
          if(task.isSuccessful()) {
              Toast.makeText(FirstRegistration.this, "Registered Successfully", Toast.LENGTH_SHORT).show();
              loginUser();
              nextActivity();
        } else {
//              FirebaseAuthException e = (FirebaseAuthException) task.getException();
           //   Toast.makeText(FirstRegistration.this, "Failed Registration: "+e.getMessage(), Toast.LENGTH_SHORT).show();
              Toast.makeText(FirstRegistration.this, "Could not register user, please try again", Toast.LENGTH_SHORT).show();
            //  nextActivity();
        }
        }
        });
    }

    private void loginUser(){
        String email = editTextEmailReg.getText().toString().trim();
        String password  = editTextPasswordReg.getText().toString().trim();

        //logging in the user
        firebaseAuth.signInWithEmailAndPassword(email, password)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        progressDialog.dismiss();
                        //if the task is successfull
                        if(task.isSuccessful()){
                            //start the profile activity
                            finish();
                            startActivity(new Intent(getApplicationContext(), Registration.class));
                        }
                    }
                });
    }

    private boolean checkPassword(String pass, String confirmPassword) {
        if (pass.equals(confirmPassword)) {
            return true;
        }
        return false;
    }

    @Override
    public void onClick(View v) {
        if (v == buttonRegister) {
                registerUser();
                progressDialog.dismiss();
        }
    }

    private void nextActivity() {
        finish();
        startActivity(new Intent(this, Registration.class));
    }
}