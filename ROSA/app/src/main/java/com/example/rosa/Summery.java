package com.example.rosa;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.rosa.constans.Constans;


public class Summery extends Fragment {
    ProgressDialog progressDialog;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        progressDialog=new ProgressDialog(getContext());
        progressDialog.setMessage("Getting info. Please wait..");
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();
        View view=inflater.inflate(R.layout.fragment_summery, container, false);
        TextView cby=view.findViewById(R.id.cby);

        TextView esd=view.findViewById(R.id.esd);


        TextView eed=view.findViewById(R.id.eed);

        TextView users=view.findViewById(R.id.nuser);


        TextView mAmount=view.findViewById(R.id.mamount);
        TextView mPAmount=view.findViewById(R.id.mpamount);
        TextView createdBy=view.findViewById(R.id.createdby);
        cby.setText(Constans.rosaModel.rosaName);
        esd.setText(Constans.rosaModel.startdate);
        eed.setText(Constans.rosaModel.enddate);
        users.setText(Constans.rosaModel.numberOfUsers);
        TextView m1,m2,m3,m4,m5,m6;
        m1=view.findViewById(R.id.m1);

        m2=view.findViewById(R.id.m2);

        m3=view.findViewById(R.id.m3);

        m4=view.findViewById(R.id.m4);

        m5=view.findViewById(R.id.m5);

        m6=view.findViewById(R.id.m6);
        if(Constans.rosaModel.UserPositions.get(0)!=null)
        m1.setText(Constans.rosaModel.UserPositions.get(0).FirstName1+" "+Constans.rosaModel.UserPositions.get(0).LastName1);
        else{
            m1.setText("Not Available");


        }

        if(Constans.rosaModel.UserPositions.get(1)!=null)
        m2.setText(Constans.rosaModel.UserPositions.get(1).FirstName1+" "+Constans.rosaModel.UserPositions.get(1).LastName1);
        else{
            m2.setText("Not Available");


        }

        if(Constans.rosaModel.UserPositions.get(2)!=null)
        m3.setText(Constans.rosaModel.UserPositions.get(2).FirstName1+" "+Constans.rosaModel.UserPositions.get(2).LastName1);
        else{
            m3.setText("Not Available");


        }

        if(Constans.rosaModel.UserPositions.get(3)!=null)
        m4.setText(Constans.rosaModel.UserPositions.get(3).FirstName1+" "+Constans.rosaModel.UserPositions.get(3).LastName1);
        else{
            m4.setText("Not Available");


        }

        if(Constans.rosaModel.UserPositions.get(4)!=null)
        m5.setText(Constans.rosaModel.UserPositions.get(4).FirstName1+" "+Constans.rosaModel.UserPositions.get(4).LastName1);
        else{
            m5.setText("Not Available");


        }

        if(Constans.rosaModel.UserPositions.get(5)!=null)
        m6.setText(Constans.rosaModel.UserPositions.get(5).FirstName1+" "+Constans.rosaModel.UserPositions.get(5).LastName1);
        else{
            m6.setText("Not Available");


        }

        createdBy.setText(Constans.rosaModel.user.FirstName1+" "+Constans.rosaModel.user.LastName1);

        mAmount.setText("£"+Constans.rosaModel.amount);
        mPAmount.setText("£"+Integer.parseInt(Constans.rosaModel.amount)*Integer.parseInt(Constans.rosaModel.numberOfUsers));
         progressDialog.dismiss();
        return view;
    }
}
