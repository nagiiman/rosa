package com.example.rosa;

import com.example.rosa.model.ChatModel;

import java.io.Serializable;
import java.util.List;

import co.intentservice.chatui.models.ChatMessage;

public class ROSAModel implements Serializable {

    private static final long serialversionUID =
            129348938L;
    public String rosaName;
    public String numberOfUsers;
    public String amount;
    public Boolean Cash;
    public String startdate;
    public String enddate;
    public List<User> UserPositions;
    public String postion;
    public int joineduser=0;
   //public List<User> ROSAUsers;
   public List<ChatModel> chat;
    public User user;
    public boolean started=false;
    public boolean complete=false;

    public ROSAModel() {
    }

    public ROSAModel (String nameROSA, String numberOfUsersROSA, String amountROSA, boolean cash, List<User> userPositions,String startdate,String enddate,String postion,User user,List<ChatModel> chat,boolean complete,boolean started,int joineduser) {
        rosaName = nameROSA;
        numberOfUsers = numberOfUsersROSA;
        amount = amountROSA;
        Cash = cash;
        UserPositions = userPositions;
        this.startdate=startdate;
        this.enddate=enddate;
        this.postion=postion;
        this.user=user;
        this.chat=chat;
        this.complete=complete;
        this.started=started;
        this.joineduser=joineduser;
    }
    public ROSAModel (String rosaID, String numberOfUsersROSA, String amountROSA, boolean cash) {
        rosaName = rosaID;
        numberOfUsers = numberOfUsersROSA;
        amount = amountROSA;
        Cash = cash;
    }

    public String getRosaName() {
        return rosaName;
    }

    public String getNumberOfUsers() {
        return numberOfUsers;
    }

    public String getAmount() {
        return amount;
    }
}
