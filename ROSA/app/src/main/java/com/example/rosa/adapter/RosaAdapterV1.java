package com.example.rosa.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.rosa.R;
import com.example.rosa.ROSAModel;
import com.example.rosa.RosaAdapter;
import com.example.rosa.RosaDetails;
import com.example.rosa.constans.Constans;

import java.util.List;

public class RosaAdapterV1 extends RecyclerView.Adapter<RosaAdapterV1.RosaHolder>{
    @Override
    public void onBindViewHolder(@NonNull RosaHolder holder, int position, @NonNull List<Object> payloads) {
        super.onBindViewHolder(holder, position, payloads);
    }

    List<ROSAModel> rosaModelList;
    Context context;
    public RosaAdapterV1(List<ROSAModel> rosaModelList, Context context){
        this.rosaModelList=rosaModelList;
        this.context=context;
    }
/*
    @Override
    protected void onBindViewHolder(@NonNull final RosaAdapterV1.RosaHolder holder, int position, @NonNull final ROSAModel model) {
        holder.textViewTitle.setText(model.getRosaName());
        holder.textViewNumberOfUsers.setText(model.getNumberOfUsers());
        holder.textViewAmount.setText(model.getAmount());
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Constans.rosaModel=model;
                Intent intent = new Intent(holder.itemView.getContext(), RosaDetails.class);
                holder.itemView.getContext().startActivity(intent);

            }
        });


    }
*/


    @NonNull
    @Override
    public RosaAdapterV1.RosaHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.layout_listitem, viewGroup, false);
        return new RosaAdapterV1.RosaHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull final RosaHolder holder, int i) {

        final ROSAModel model=rosaModelList.get(i);
        holder.textViewTitle.setText(model.getRosaName());
        holder.textViewNumberOfUsers.setText(model.getNumberOfUsers());
        holder.textViewAmount.setText(model.getAmount());

        holder.createdBy.setText("Joined and Created By "+model.user.FirstName1+" "+model.user.LastName1);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Constans.rosaModel=model;
                Intent intent = new Intent(holder.itemView.getContext(), RosaDetails.class);
                holder.itemView.getContext().startActivity(intent);

            }
        });





    }



    @Override
    public int getItemCount() {
        return rosaModelList.size();
    }

    public class RosaHolder extends RecyclerView.ViewHolder{

        TextView textViewTitle;
        TextView textViewNumberOfUsers;
        TextView textViewAmount;
        TextView createdBy;

        public RosaHolder(@NonNull final View itemView) {
            super(itemView);
            textViewTitle = itemView.findViewById(R.id.textViewTitle);
            textViewNumberOfUsers = itemView.findViewById(R.id.textViewNumberOfUsers);
            textViewAmount = itemView.findViewById(R.id.textViewAmount);
             createdBy=itemView.findViewById(R.id.createdby);
        }
    }



}
