package com.example.rosa;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.rosa.constans.Constans;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.FirebaseFirestore;

public class RateUser extends AppCompatActivity {
    RatingBar m1r, m2r, m3r, m4r, m5r, m6r;
    TextView m1, m2, m3, m4, m5, m6;
    ProgressDialog progressDialog;
    private FirebaseFirestore firebaseFirestore;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rate_user);
        firebaseFirestore = firebaseFirestore.getInstance();
        progressDialog = new ProgressDialog(this);
        m1r = findViewById(R.id.m1r);
        m2r = findViewById(R.id.m2r);
        m3r = findViewById(R.id.m3r);
        m4r = findViewById(R.id.m4r);
        m5r = findViewById(R.id.m5r);
        m6r = findViewById(R.id.m6r);
        m1 = findViewById(R.id.m1);
        m2 = findViewById(R.id.m2);
        m3 = findViewById(R.id.m3);
        m4 = findViewById(R.id.m4);
        m5 = findViewById(R.id.m5);
        m6 = findViewById(R.id.m6);

        m1.setText(Constans.rosaModel.UserPositions.get(0).FirstName1 + " " + Constans.rosaModel.UserPositions.get(0).LastName1);
        m2.setText(Constans.rosaModel.UserPositions.get(1).FirstName1 + " " + Constans.rosaModel.UserPositions.get(1).LastName1);
        m3.setText(Constans.rosaModel.UserPositions.get(2).FirstName1 + " " + Constans.rosaModel.UserPositions.get(2).LastName1);
        m4.setText(Constans.rosaModel.UserPositions.get(3).FirstName1 + " " + Constans.rosaModel.UserPositions.get(3).LastName1);
        m5.setText(Constans.rosaModel.UserPositions.get(4).FirstName1 + " " + Constans.rosaModel.UserPositions.get(4).LastName1);
        m6.setText(Constans.rosaModel.UserPositions.get(5).FirstName1 + " " + Constans.rosaModel.UserPositions.get(5).LastName1);


    }

    public void submit(View view) {
        progressDialog = new ProgressDialog(getApplicationContext());
        progressDialog.setMessage("Updating info. Please wait..");
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();
        firebaseFirestore = firebaseFirestore.getInstance();
        float r1 = m1r.getRating();
        float r2 = m2r.getRating();
        float r3 = m3r.getRating();
        float r4 = m4r.getRating();
        float r5 = m5r.getRating();
        float r6 = m6r.getRating();
        User user1 = Constans.rosaModel.UserPositions.get(0);

        User user2 = Constans.rosaModel.UserPositions.get(1);

        User user3 = Constans.rosaModel.UserPositions.get(2);
        User user4 = Constans.rosaModel.UserPositions.get(3);
        User user5 = Constans.rosaModel.UserPositions.get(4);
        User user6 = Constans.rosaModel.UserPositions.get(5);
        user1.rating = "" + r1;
        user2.rating = "" + r2;
        user3.rating = "" + r3;
        user4.rating = "" + r4;
        user5.rating = "" + r5;
        user6.rating = "" + r6;


        firebaseFirestore.collection("users").document(user1.EmailAddress1).set(user1).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                //Toast.makeText(Settings.this.getContext(), " Successfully", Toast.LENGTH_SHORT).show();

            }
        })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Toast.makeText(RateUser.this, "Failed Registration: " + e.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });


        firebaseFirestore.collection("users").document(user2.EmailAddress1).set(user2).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                //Toast.makeText(Settings.this.getContext(), " Successfully", Toast.LENGTH_SHORT).show();


            }
        })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Toast.makeText(RateUser.this, "Failed Registration: " + e.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });


        firebaseFirestore.collection("users").document(user3.EmailAddress1).set(user3).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                //Toast.makeText(Settings.this.getContext(), " Successfully", Toast.LENGTH_SHORT).show();


            }
        })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Toast.makeText(RateUser.this, "Failed Registration: " + e.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });


        firebaseFirestore.collection("users").document(user4.EmailAddress1).set(user4).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                //Toast.makeText(Settings.this.getContext(), " Successfully", Toast.LENGTH_SHORT).show();

            }
        })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Toast.makeText(RateUser.this, "Failed Registration: " + e.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });


        firebaseFirestore.collection("users").document(user5.EmailAddress1).set(user5).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                //Toast.makeText(Settings.this.getContext(), " Successfully", Toast.LENGTH_SHORT).show();


            }
        })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Toast.makeText(RateUser.this, "Failed Registration: " + e.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });


        firebaseFirestore.collection("users").document(user6.EmailAddress1).set(user6).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                //Toast.makeText(Settings.this.getContext(), " Successfully", Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();
                onBackPressed();

            }
        })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Toast.makeText(RateUser.this, "Failed Registration: " + e.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });

    }
}
