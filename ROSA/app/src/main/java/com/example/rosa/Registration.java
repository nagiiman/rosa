package com.example.rosa;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.ArrayList;

public class Registration extends AppCompatActivity implements View.OnClickListener {

    private EditText editTextFirstName;
    private EditText editTextLastName;
    private EditText editTextPhoneNumber;
    private EditText editTextAddressL1;
    private EditText editTextAddressL2;
    private AutoCompleteTextView autoCompleteTextViewCity;
    private AutoCompleteTextView autoCompleteTextViewCountry;
    private EditText editTextDOB;
    private Button buttonSignUp;

    private ProgressDialog progressDialog;

    private FirebaseAuth firebaseAuth;
    private FirebaseFirestore firebaseFirestore;

    private static final String TAG = "Registration";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);

        firebaseAuth = FirebaseAuth.getInstance();
        firebaseFirestore = firebaseFirestore.getInstance();

        editTextFirstName = (EditText) findViewById(R.id.editTextFirstName);
        editTextLastName = (EditText) findViewById(R.id.editTextLastName);
        editTextPhoneNumber = (EditText) findViewById(R.id.editTextPhoneNumber);
        editTextAddressL1 = (EditText) findViewById(R.id.editTextAddressL1);
        editTextAddressL2 = (EditText) findViewById(R.id.editTextAddressL2);
        autoCompleteTextViewCity = (AutoCompleteTextView) findViewById(R.id.autoCompleteTextViewCity);
        autoCompleteTextViewCountry = (AutoCompleteTextView) findViewById(R.id.autoCompleteTextViewCountry);
        editTextDOB = (EditText) findViewById(R.id.editTextDOB);

        buttonSignUp = (Button) findViewById(R.id.buttonSignUp);
        buttonSignUp.setOnClickListener(this);
    }

    private void saveUserInformation() {
        //String uid =  firebaseAuth.getCurrentUser().getUid();
        String emailAddress = firebaseAuth.getCurrentUser().getEmail();
        String firstName = editTextFirstName.getText().toString().trim();
        String lastName = editTextLastName.getText().toString().trim();
        String phoneNumber = editTextPhoneNumber.getText().toString().trim();
        String addressL1 = editTextAddressL1.getText().toString().trim();
        String addressL2 = editTextAddressL2.getText().toString().trim();
        String city = autoCompleteTextViewCity.getText().toString().trim();
        String country = autoCompleteTextViewCountry.getText().toString().trim();
        String dob = editTextDOB.getText().toString().trim();


        User user = new User(firstName, emailAddress, lastName, phoneNumber, addressL1, addressL2, city, country, dob, "", new ArrayList<String>(), "none", "0.0");

        String registeredEmail = firebaseAuth.getCurrentUser().getEmail();
       /* CollectionReference dbUsers = firebaseFirestore.collection("users");

        dbUsers.add(user).addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
            @Override
            public void onSuccess(DocumentReference documentReference) {
                Toast.makeText(Registration.this, "Registered Successfully", Toast.LENGTH_SHORT).show();
                nextActivity();
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Toast.makeText(Registration.this, "Failed Registration: "+e.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });*/

        firebaseFirestore.collection("users").document(registeredEmail).set(user).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                Log.d(TAG, "DocumentSnapshot successfully written!");
                Toast.makeText(Registration.this, "Registered Successfully", Toast.LENGTH_SHORT).show();
                nextActivity();
            }
        })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.w(TAG, "Error writing document", e);
                        Toast.makeText(Registration.this, "Failed Registration: " + e.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });
    }

    @Override
    public void onClick(View v) {
        if (v == buttonSignUp) {
            saveUserInformation();
            //nextActivity();
        }
    }

    private void nextActivity() {
        finish();
        startActivity(new Intent(this, Home.class));
    }

}
