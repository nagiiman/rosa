package com.example.rosa;

import java.io.Serializable;
import java.util.List;

public class User implements Serializable {

    private static final long serialversionUID =
            129348938L;
    public String FirstName1;
    public String LastName1;
    public String EmailAddress1;
    public String Phonenumber1;
    public String Address11;
    public String Address21;
    public String City1;
    public String Country1;
    public String DOB1;
    public String ROSAPosition;
    public String role; //Admin User
    public List<String> joinedRosa;
    public String createdRosa;
    public String rating;




    public User() {

    }

    public User(String firstName, String emailAddress, String lastName, String phoneNumber, String address1, String address2,  String city, String country, String dob) {

        EmailAddress1 = emailAddress;
        FirstName1 = firstName;
        LastName1 = lastName;
        Phonenumber1 = phoneNumber;
        Address11 = address1;
        Address21 = address2;
        City1 = city;
        Country1 = country;
        DOB1 = dob;
    }

    public User(String firstName1, String lastName1, String emailAddress1, String phonenumber1, String address11, String address21, String city1, String country1, String DOB1, String ROSAPosition, String role ) {
        FirstName1 = firstName1;
        LastName1 = lastName1;
        EmailAddress1 = emailAddress1;
        Phonenumber1 = phonenumber1;
        Address11 = address11;
        Address21 = address21;
        City1 = city1;
        Country1 = country1;
        this.DOB1 = DOB1;
        this.ROSAPosition = ROSAPosition;
        this.role = role;


    }

    ///
    public User(String firstName, String emailAddress, String lastName, String phoneNumber, String address1, String address2,  String city, String country, String dob, String rosaPosition,List<String> joinedRosa,String createdRosa,String rating) {
        EmailAddress1 = emailAddress;
        FirstName1 = firstName;
        LastName1 = lastName;
        Phonenumber1 = phoneNumber;
        Address11 = address1;
        Address21 = address2;
        City1 = city;
        Country1 = country;
        DOB1 = dob;
        ROSAPosition = rosaPosition;
        this.joinedRosa=joinedRosa;
        this.createdRosa=createdRosa;
        this.rating=rating;
    }
}
