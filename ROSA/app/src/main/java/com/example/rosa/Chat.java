package com.example.rosa;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.rosa.constans.Constans;
import com.example.rosa.model.ChatModel;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.firestore.FirebaseFirestore;

import java.sql.Timestamp;

import co.intentservice.chatui.ChatView;
import co.intentservice.chatui.models.ChatMessage;


public class Chat extends Fragment {
    private DatabaseReference mDatabase;
    private FirebaseAuth mAuth;
    public ProgressDialog progressDialog;
    private FirebaseFirestore firebaseFirestore;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mAuth = FirebaseAuth.getInstance();
        firebaseFirestore = firebaseFirestore.getInstance();

        FirebaseUser firebaseUser = mAuth.getCurrentUser();
        // Inflate the layout for this fragment
        progressDialog = new ProgressDialog(getContext());
        progressDialog.setMessage("Checking new messages..");
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();
        mDatabase = FirebaseDatabase.getInstance().getReference("Messages");
        View view = inflater.inflate(R.layout.fragment_chat, container, false);

        final ChatView chatView = view.findViewById(R.id.chat_view);

        for (ChatModel chatModel : Constans.rosaModel.chat) {
            if (chatModel != null && chatModel.sender.equalsIgnoreCase(Constans.user.FirstName1)) {
                chatView.addMessage(new ChatMessage(chatModel.message, chatModel.time, ChatMessage.Type.SENT, chatModel.sender));

            } else if (chatModel != null) {
                chatView.addMessage(new ChatMessage(chatModel.message, chatModel.time, ChatMessage.Type.RECEIVED, chatModel.sender));


            } else {
                Timestamp timestamp = new Timestamp(System.currentTimeMillis());

                chatView.addMessage(new ChatMessage("Chat is Empty!", timestamp.getTime(), ChatMessage.Type.RECEIVED, "ROSA"));


            }

        }
        chatView.setOnSentMessageListener(new ChatView.OnSentMessageListener() {
            @Override
            public boolean sendMessage(ChatMessage chatMessage) {
                // perform actual message sending
                ChatModel chatModel = new ChatModel(chatMessage.getMessage(), chatMessage.getType(), chatMessage.getTimestamp(), Constans.user.FirstName1);
                Constans.rosaModel.chat.add(chatModel);
                mDatabase.child(Constans.rosaModel.rosaName).setValue(chatModel);
                firebaseFirestore.collection("rosa").document(Constans.rosaModel.rosaName).set(Constans.rosaModel).addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {


                    }
                })
                        .addOnFailureListener(new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception e) {

                            }
                        });

                return true;
            }
        });
        mDatabase.child(Constans.rosaModel.rosaName).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                ChatModel chatModel = dataSnapshot.getValue(ChatModel.class);
                if (chatModel != null && chatModel.sender.equalsIgnoreCase(Constans.user.FirstName1)) {

                } else if (chatModel != null) {
                    chatView.addMessage(new ChatMessage(chatModel.message, chatModel.time, ChatMessage.Type.RECEIVED, chatModel.sender));


                } else {
                    Timestamp timestamp = new Timestamp(System.currentTimeMillis());

                    chatView.addMessage(new ChatMessage("Chat is Empty!", timestamp.getTime(), ChatMessage.Type.RECEIVED, "ROSA"));


                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
        progressDialog.dismiss();
        return view;
    }
}
