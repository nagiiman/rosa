package com.example.rosa;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.rosa.constans.Constans;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.FirebaseFirestore;

public class EditPosition extends AppCompatActivity {
    private static final String TAG = "Firebase";
    EditText cpos, ccpos;
    private FirebaseFirestore firebaseFirestore;
    ProgressDialog progressDialog;
    String currentPosition;
    int tempCurrentPosition = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_position);
        ccpos = findViewById(R.id.ccpos);
        int i = 1;
        for (User user : Constans.rosaModel.UserPositions) {
            if (user != null && user.FirstName1.equalsIgnoreCase(Constans.user.FirstName1) && user.LastName1.equalsIgnoreCase(Constans.user.LastName1)) {
                currentPosition = "" + i;
                System.out.println("Position " + i);
                tempCurrentPosition = i - 1;
            }
            i++;


        }
        ccpos.setText(currentPosition);
        firebaseFirestore = firebaseFirestore.getInstance();
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Saving informaiton Please Wait..");
        progressDialog.setCanceledOnTouchOutside(false);
    }

    public void save(int position) {
        progressDialog.show();

        Constans.rosaModel.UserPositions.add(tempCurrentPosition, null);

        Constans.rosaModel.UserPositions.add(position, Constans.user);
        firebaseFirestore.collection("rosa").document(Constans.rosaModel.rosaName).set(Constans.rosaModel).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                Log.d(TAG, "DocumentSnapshot successfully written!");
                Toast.makeText(EditPosition.this, "Position Changed Successfully", Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();
                onBackPressed();

            }
        })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.w(TAG, "Error writing document", e);
                        Toast.makeText(EditPosition.this, "Failed: " + e.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });


    }

    public void viewPosition(View view) {

        Dialog dialog = new Dialog(this);

        dialog.setContentView(R.layout.change_position_dialog);
        TextView m1, m2, m3, m4, m5, m6;
        m1 = (TextView) dialog.findViewById(R.id.m1);
        m2 = (TextView) dialog.findViewById(R.id.m2);
        m3 = (TextView) dialog.findViewById(R.id.m3);
        m4 = (TextView) dialog.findViewById(R.id.m4);
        m5 = (TextView) dialog.findViewById(R.id.m5);
        m6 = (TextView) dialog.findViewById(R.id.m6);
        if (Constans.rosaModel.UserPositions.get(0) != null) {
            m1.setText(Constans.rosaModel.UserPositions.get(0).FirstName1 + " " + Constans.rosaModel.UserPositions.get(0).LastName1);

        } else {
            m1.setText("Available");
            m1.setTextColor(Color.BLUE);
            m1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    save(0);
                }
            });


        }

        if (Constans.rosaModel.UserPositions.get(1) != null) {
            m2.setText(Constans.rosaModel.UserPositions.get(1).FirstName1 + " " + Constans.rosaModel.UserPositions.get(1).LastName1);


        } else {
            m2.setText("Available");
            m2.setTextColor(Color.BLUE);
            m2.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    save(1);
                }
            });

        }

        if (Constans.rosaModel.UserPositions.get(2) != null) {
            m3.setText(Constans.rosaModel.UserPositions.get(2).FirstName1 + " " + Constans.rosaModel.UserPositions.get(2).LastName1);

        } else {
            m3.setText("Available");
            m3.setTextColor(Color.BLUE);
            m3.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    save(2);
                }
            });

        }

        if (Constans.rosaModel.UserPositions.get(3) != null) {
            m4.setText(Constans.rosaModel.UserPositions.get(3).FirstName1 + " " + Constans.rosaModel.UserPositions.get(3).LastName1);

        } else {
            m4.setText("Available");
            m4.setTextColor(Color.BLUE);
            m4.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    save(3);
                }
            });


        }

        if (Constans.rosaModel.UserPositions.get(4) != null) {
            m5.setText(Constans.rosaModel.UserPositions.get(4).FirstName1 + " " + Constans.rosaModel.UserPositions.get(4).LastName1);


        } else {

            m5.setText(" Available");
            m5.setTextColor(Color.BLUE);
            m5.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    save(4);
                }
            });


        }

        if (Constans.rosaModel.UserPositions.get(5) != null) {
            m6.setText(Constans.rosaModel.UserPositions.get(5).FirstName1 + " " + Constans.rosaModel.UserPositions.get(5).LastName1);

        } else {
            m6.setText("Available");

            m6.setTextColor(Color.BLUE);
            m6.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    save(5);
                }
            });


        }


        dialog.setTitle("Change Position");
        dialog.show();


    }

}
