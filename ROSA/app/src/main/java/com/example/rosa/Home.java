package com.example.rosa;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.example.rosa.adapter.RosaAdapterV1;
import com.example.rosa.constans.Constans;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.ArrayList;
import java.util.List;

public class Home extends AppCompatActivity implements View.OnClickListener {

    private TextView textViewFirstName;
    private TextView textViewLogout, userating;
    private TextView textViewSearch;
    private Button buttonROSA1;
    private FirebaseAuth firebaseAuth;
    private FirebaseFirestore firebaseFirestore = FirebaseFirestore.getInstance();
    private String firstName;
    private FloatingActionButton buttonROSA;
    private DocumentReference rosaRef;
    private List<ROSAModel> joinRosa;

    private RosaAdapterV1 adapter;

    private static final String TAG = "Home";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        joinRosa = new ArrayList<>();
        adapter = new RosaAdapterV1(joinRosa, getApplicationContext());
        textViewFirstName = (TextView) findViewById(R.id.textViewWelcome);
        textViewSearch = (TextView) findViewById(R.id.textViewSearch);
        firebaseAuth = FirebaseAuth.getInstance();
        userating = findViewById(R.id.userrating);
        buttonROSA = (FloatingActionButton) findViewById(R.id.buttonROSA);
        textViewLogout = (TextView) findViewById(R.id.textViewLogOut);
        gettingData();
        textViewLogout.setOnClickListener(this);
        buttonROSA.setOnClickListener(this);
        textViewSearch.setOnClickListener(this);
        createCurrentUser();
        //setUpRecyclerView();


    }

    public void createCurrentUser() {
        FirebaseUser user = firebaseAuth.getCurrentUser();
        String userEmail = user.getEmail();
        final DocumentReference docRef = firebaseFirestore.collection("users").document(userEmail);
        final String docPath = docRef.getPath();
        docRef.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                if (task.isSuccessful()) {
                    final DocumentSnapshot document = task.getResult();
                    if (document.exists()) {

                        String firstName = document.getData().get("FirstName1").toString();
                        String lastName = document.getData().get("LastName1").toString();
                        String emaail = document.getData().get("EmailAddress1").toString();
                        String phone = document.getData().get("Phonenumber1").toString();
                        final String dob = document.getData().get("DOB1").toString();
                        String add = document.getData().get("Address11").toString();
                        String add2 = document.getData().get("Address21").toString();
                        String city = document.getData().get("City1").toString();
                        String country = document.getData().get("Country1").toString();
                        final List<String> joindRosa = (List<String>) document.getData().get("joinedRosa");
                        String createdRosa = document.getData().get("createdRosa").toString();
                        String rating = document.getData().get("rating").toString();

                        User user = new User(firstName, emaail, lastName, phone, add, add2, city, country, dob, "", joindRosa, createdRosa, rating);

                        Constans.user = user;
                        for (String joinRosa : Constans.user.joinedRosa) {
                            rosaRef = firebaseFirestore.collection("rosa").document(joinRosa);
                            rosaRef.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                                @Override
                                public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                                    DocumentSnapshot document = task.getResult();
                                    ROSAModel rosaModel = document.toObject(ROSAModel.class);
                                    Home.this.joinRosa.add(rosaModel);
                                    adapter.notifyDataSetChanged();
                                }
                            });
                        }
                        if (joindRosa.size() > 0)
                            setUpRecyclerView();

                    }
                }
            }
        });
    }

    private void setUpRecyclerView() {
        adapter = new RosaAdapterV1(joinRosa, getApplicationContext());
        System.out.println("adfasd" + adapter.getItemCount());
        RecyclerView recyclerView = findViewById(R.id.recycleViewROSA);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(adapter);
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    private void gettingData() {
        FirebaseUser user = firebaseAuth.getCurrentUser();
        String userEmail = user.getEmail();
        final DocumentReference docRef = firebaseFirestore.collection("users").document(userEmail);
        docRef.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                if (task.isSuccessful()) {
                    DocumentSnapshot document = task.getResult();
                    if (document.exists()) {
                        Log.d(TAG, "Document Snapshot data: " + document.getData());
                        firstName = document.getData().get("FirstName1").toString();
                        String rating = document.getData().get("rating").toString();
                        userating.setText("Current Rating " + rating);
                        textViewFirstName.setText("Welcome " + firstName);


                    } else {
                        Log.d(TAG, "No such document");
                    }
                } else {
                    Log.d(TAG, "get failed with ", task.getException());
                }
            }
        });
    }

    @Override
    public void onClick(View v) {
        if (v == textViewLogout) {
            //logging out the user
            firebaseAuth.signOut();
            //closing activity
            finish();
            //starting login activity
            startActivity(new Intent(this, MainActivity.class));
        }
        if (v == buttonROSA) {
            startActivity(new Intent(this, ROSA.class));
        }
        if (v == textViewSearch) {
            startActivity(new Intent(this, SearchROSA.class));
        }
    }
}