package com.example.rosa;

import android.app.ProgressDialog;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.example.rosa.constans.Constans;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.FirebaseFirestore;

public class EditStartDate extends AppCompatActivity {
    private static final String TAG ="Firebase" ;
    EditText edate,sdate;
    private FirebaseFirestore firebaseFirestore;
 ProgressDialog progressDialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_start_date);
         edate=findViewById(R.id.edate);
         sdate=findViewById(R.id.sdate);
         edate.setText(Constans.rosaModel.enddate);
         sdate.setText(Constans.rosaModel.startdate);
        firebaseFirestore = firebaseFirestore.getInstance();
         progressDialog=new ProgressDialog(this);
         progressDialog.setMessage("Saving informaiton Please Wait..");
         progressDialog.setCanceledOnTouchOutside(false);
    }
    public void save(View view){
        progressDialog.show();
        Constans.rosaModel.enddate=edate.getText().toString();
        Constans.rosaModel.startdate=sdate.getText().toString();
        firebaseFirestore.collection("rosa").document(Constans.rosaModel.rosaName).set(Constans.rosaModel).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                Log.d(TAG, "DocumentSnapshot successfully written!");
                Toast.makeText(EditStartDate.this, "Start Date Change Successfully", Toast.LENGTH_SHORT).show();
                   progressDialog.dismiss();
                   onBackPressed();

            }
        })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.w(TAG, "Error writing document", e);
                        Toast.makeText(EditStartDate.this, "Failed: "+e.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });


    }

}
